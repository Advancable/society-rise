package com.socrise.game;

import android.os.Bundle;

import com.advancable.socrise.TheGame;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

/**
 * Android backend.
 */
public class AndroidLauncher extends AndroidApplication {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
        config.useAccelerometer = false;
        config.useImmersiveMode = false;
        config.useWakelock = false;
        TheGame.setAssetsPath("");
        initialize(TheGame.getInstance(), config);
    }
}
