package com.advancable.socrise.Model.Screens;

import com.advancable.socrise.Controller.MenuScreen.Help_Listener;
import com.advancable.socrise.Controller.MenuScreen.Options_Listener;
import com.advancable.socrise.Controller.MenuScreen.Play_Listener;
import com.advancable.socrise.TheGame;
import com.advancable.socrise.View.ButtonActor;
import com.advancable.socrise.View.LineActor;
import com.advancable.socrise.View.TextureActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

/**
 * @author Somkin Ivan
 *         It's a main menu.
 */
public class Menu_Screen implements Screen {
    private Batch batch;
    private static Music bgm;
    ButtonActor play;
    ButtonActor options;
    ButtonActor help;
    TextureActor title;
    LineActor[] lines;
    private Stage stage;
    public static Music getBgm(){
        return bgm;
    }
    public void updateTextures(){
        stage.clear();
        title = new TextureActor(new TextureRegion(TheGame.getTextures(), 0, 0, 386, 225), Gdx.graphics.getWidth() / 2f - 193, Gdx.graphics.getHeight() / 2f + 60, 386, 225);
        play = new ButtonActor(new TextureRegion(TheGame.getTextures(), 386, 0, 308, 102), new TextureRegion(TheGame.getTextures(), 386, 307, 308, 102), Gdx.graphics.getWidth() / 2f - 154, Gdx.graphics.getHeight() / 2f-80 , 308, 101);
        options = new ButtonActor(new TextureRegion(TheGame.getTextures(), 386, 102, 308, 103), new TextureRegion(TheGame.getTextures(), 386, 408, 308, 103), Gdx.graphics.getWidth() / 2f - 154, Gdx.graphics.getHeight() / 2f - 183, 308, 102);
        help = new ButtonActor(new TextureRegion(TheGame.getTextures(), 386, 204, 308, 103), new TextureRegion(TheGame.getTextures(), 386, 510, 308, 103), Gdx.graphics.getWidth() / 2f - 154, Gdx.graphics.getHeight() / 2f - 284, 308, 102);
        addListeners();
        for (int i = 0; i < 15; i++) {
            lines[i] = new LineActor(0,Gdx.graphics.getHeight()+ (Gdx.graphics.getHeight() / 15f) * i, Gdx.graphics.getWidth(), 0, 0.5f, 0.5f, 0.5f, 1,1);
            stage.addActor(lines[i]);
        }
        stage.addActor(title);
        stage.addActor(play);
        stage.addActor(options);
        stage.addActor(help);

    }
    public Menu_Screen(SpriteBatch batch) {
        this.batch=batch;
        bgm = Gdx.audio.newMusic(Gdx.files.internal(TheGame.getAssetsPath() + "music\\menu.mp3"));
    }

    private void addListeners() {
        play.addListener(new Play_Listener());
        options.addListener(new Options_Listener());
        help.addListener(new Help_Listener());
    }

    @Override
    public void show() {
        stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()), batch);
        lines = new LineActor[15];
        updateTextures();
        Gdx.input.setInputProcessor(stage);
        stage.act(2);
    }

    @Override
    public void render(float delta) {
        for (int i = 0; i < 15; i++) {
            if (lines[i].getY() > 0) {
                lines[i].setPosition(0,lines[i].getY() - 3);
                lines[i].setSize(Gdx.graphics.getWidth(),0);
            } else {
                lines[i].setY(Gdx.graphics.getHeight());
                lines[i].setHeight(Gdx.graphics.getHeight());
            }
        }
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    stage.dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}

