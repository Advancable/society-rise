package com.advancable.socrise.Model.Screens;

import com.advancable.socrise.Controller.MenuScreen.Options_Listener;
import com.advancable.socrise.Controller.Menu_Listener;
import com.advancable.socrise.Controller.OptionsScreen.Language_Changer;
import com.advancable.socrise.Controller.OptionsScreen.Volume_Changer;
import com.advancable.socrise.TheGame;
import com.advancable.socrise.View.ButtonActor;
import com.advancable.socrise.View.LineActor;
import com.advancable.socrise.View.TextureActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * @author Somkin Ivan.
 *         Screen that holds the options selection.
 */
public class Options_Screen implements Screen {
    private static ButtonActor back;
    private static ButtonActor backToMenu;
    Texture languages;
    LineActor[] lines;
    TextureActor arrow;
    static ButtonActor  volumeSlider;
    ButtonActor English;
    ButtonActor Russian;
    ButtonActor language;
    String[] languageNames;
    int[] languageTextureY;
    private Batch batch;
    private Stage stage;

    public Options_Screen(SpriteBatch batch) {
        this.batch = batch;
    }

    public void updateTextures() {
        stage.clear();
        for (int i = 0; i < 15; i++) {
            lines[i] = new LineActor(0, Gdx.graphics.getHeight() + (Gdx.graphics.getHeight() / 15f) * i, Gdx.graphics.getWidth(), 0, 1, 1, 1, 1, 1);
            stage.addActor(lines[i]);
        }
        back = new ButtonActor(new TextureRegion(TheGame.getTextures(), 0, 429, 308, 102), new TextureRegion(TheGame.getTextures(), 0, 531, 308, 102), Gdx.graphics.getWidth() / 2f - 154, 0, 308, 102);
        backToMenu = new ButtonActor(new TextureRegion(TheGame.getTextures(), 0, 225, 308, 102), new TextureRegion(TheGame.getTextures(), 0, 327, 308, 102), Gdx.graphics.getWidth() / 2f - 154, 0, 308, 102);
        arrow = new TextureActor(new TextureRegion(TheGame.getTextures(), 1144, 0, 451, 619), Gdx.graphics.getWidth() / 2f - 225, Gdx.graphics.getHeight() / 2f - 300, 451, 619);
        volumeSlider = new ButtonActor(new TextureRegion(TheGame.getTextures(), 0, 852, 51, 47), new TextureRegion(TheGame.getTextures(), 0, 898, 51, 47), Gdx.graphics.getWidth() / 2f - 175 + 300 * TheGame.volume, Gdx.graphics.getHeight() / 2f + 27, 51, 47);
        System.out.println(TheGame.volume);
        int tmp = 0;
        for (int i = 0; i < languageNames.length; i++)
            if (languageNames[i].equals(TheGame.getLanguage()))
                tmp = i;
        language = new ButtonActor(new TextureRegion(languages, 0, 140 + languageTextureY[tmp], 300, 70), new TextureRegion(languages, 0, languageTextureY[tmp], 300, 70), Gdx.graphics.getWidth() / 2f - 149, Gdx.graphics.getHeight() / 2f + 148, 300, 72);

        stage.addActor(arrow);
        stage.addActor(language);
        stage.addActor(backToMenu);
        stage.addActor(back);
        stage.addActor(English);
        stage.addActor(Russian);
        stage.addActor(volumeSlider);
        addListeners();
    }

    private void addListeners() {
        backToMenu.addListener(new Menu_Listener());
        back.addListener(new Options_Listener());
        language.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.getTarget().act(1);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                if (event.getTarget() == event.getListenerActor().hit(x, y, true)) {
                    Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s", TheGame.getAssetsPath(), "sound\\menu button.mp3")));
                    sound.play(TheGame.volume);
                    Timer timer = new Timer();
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            back.setVisible(true);
                            Russian.setVisible(true);
                            English.setVisible(true);
                            language.setVisible(false);
                            arrow.setVisible(false);
                            backToMenu.setVisible(false);
                            volumeSlider.setVisible(false);
                            for (int i = 0; i < 15; i++)
                                lines[i].setVisible(false);

                        }

                    }, 0.1f);
                } else {
                    event.getTarget().act(2);
                }

            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (event.getTarget() == event.getListenerActor().hit(x, y, true))
                    event.getTarget().act(1);
                else
                    event.getTarget().act(2);
            }
        });
    }

    @Override
    public void show() {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        lines = new LineActor[15];
        languages = new Texture(String.format("%s%s", TheGame.getAssetsPath(), "textures\\languages.png"));
        languageNames = new String[]{"eng", "rus"};
        languageTextureY = new int[]{0, 70};
        English = new ButtonActor(new TextureRegion(languages, 0, 0, 300, 70), new TextureRegion(languages, 0, 140, 300, 70), Gdx.graphics.getWidth() / 2f - 148, Gdx.graphics.getHeight() / 2f + 50, 300, 70);
        Russian = new ButtonActor(new TextureRegion(languages, 0, 70, 300, 70), new TextureRegion(languages, 0, 210, 300, 70), Gdx.graphics.getWidth() / 2f - 148, Gdx.graphics.getHeight() / 2f - 50, 300, 70);
        updateTextures();
        English.addListener(new Language_Changer("eng"));
        Russian.addListener(new Language_Changer("rus"));
        InputMultiplexer multiplexer=new InputMultiplexer();
        multiplexer.addProcessor(0, stage);
        multiplexer.addProcessor(1,new Volume_Changer());
        Gdx.input.setInputProcessor(multiplexer);
        stage.act(2);
        arrow.setVisible(true);
        backToMenu.setVisible(true);
        for (int i = 0; i < 15; i++)
            lines[i].setVisible(true);
        language.setVisible(true);
        back.setVisible(false);
        Russian.setVisible(false);
        English.setVisible(false);
        volumeSlider.setVisible(true);


    }
    public static void dragSlider(float x){
        if((x>=Gdx.graphics.getWidth() / 2f - 175)&&( x<=Gdx.graphics.getWidth() / 2f +125))
            volumeSlider.setX(x);
        else
        if (x<Gdx.graphics.getWidth() / 2f - 175)
            volumeSlider.setX(Gdx.graphics.getWidth() / 2f - 175);
        else
            volumeSlider.setX(Gdx.graphics.getWidth() / 2f + 125);
    }
    public static void touchDownSlider(float x){
        if((x>=Gdx.graphics.getWidth() / 2f - 175)&&( x<=Gdx.graphics.getWidth() / 2f +125))
            volumeSlider.setX(x);
        else
        if (x<Gdx.graphics.getWidth() / 2f - 175)
            volumeSlider.setX(Gdx.graphics.getWidth() / 2f - 175);
        else
            volumeSlider.setX(Gdx.graphics.getWidth() / 2f + 125);
        volumeSlider.act(1);
    }
    public static void touchUpSlider(){

        volumeSlider.act(2);
    }
    @Override
    public void render(float delta) {
        for (int i = 0; i < 15; i++) {
            if (lines[i].getY() > 0) {
                lines[i].setPosition(0, lines[i].getY() - 3);
                lines[i].setSize(Gdx.graphics.getWidth(), 0);
            } else {
                lines[i].setY(Gdx.graphics.getHeight());
                lines[i].setHeight(Gdx.graphics.getHeight());
            }
        }
        Gdx.gl.glClearColor(112f / 255f, 1, 128f / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
