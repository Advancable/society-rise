package com.advancable.socrise.Model.Screens;

import com.advancable.socrise.Controller.GraphScreen.Button_Folder_Listener;
import com.advancable.socrise.Controller.GraphScreen.Desktop_Graph_Listener;
import com.advancable.socrise.Controller.GraphScreen.Generation_Folder_Listener;
import com.advancable.socrise.Controller.GraphScreen.Next_Generation_Listener;
import com.advancable.socrise.Controller.GraphScreen.Normal_Size_Listener;
import com.advancable.socrise.Controller.Menu_Listener;
import com.advancable.socrise.Controller.GraphScreen.Phone_Graph_Listener;
import com.advancable.socrise.TheGame;
import com.advancable.socrise.View.ButtonActor;
import com.advancable.socrise.View.GraphActor;
import com.advancable.socrise.View.TextureActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * @author Somkin Ivan.
 *         Screen that holds the options selection.
 */
public class Graph_Screen implements Screen {
    private static Music bgm;
    private static GraphActor Graph;
    private static ButtonActor menu;
    private static ButtonActor oneToOne;
    private static ButtonActor showAll;
    private static ButtonActor societyState;
    private static ButtonActor nextGeneration;
    private static ButtonActor generationFolder;
    private static ButtonActor buttonFolder;
    private static TextureActor borderDown;
    private static TextureActor borderUp;
    private static TextureActor transition;
    private static boolean next_generation_transition_skipped;
    private Batch batch;
    private static Stage stage;

    public Graph_Screen(SpriteBatch batch) {
        this.batch = batch;
        bgm = Gdx.audio.newMusic(Gdx.files.internal(TheGame.getAssetsPath() + "music\\graph screen\\1.mp3"));
    }

    public static void accelerateGraph(int velocity) {
        Graph.setVelocity(velocity);
        Graph.accelerate();
    }

    public static void normalSize() {
        final Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s", TheGame.getAssetsPath(), "sound\\menu button.mp3")));
        sound.play(TheGame.volume);
        Timer timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                sound.dispose();
            }

        }, 1f);
        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Graph.normalSize();

            }

        }, 0.1f);
    }

    public static void generationFold() {
        if (nextGeneration.isVisible()) {
            generationFolder.changeTextures(new TextureRegion(TheGame.getTextures(), 1788, 619, 64, 79), new TextureRegion(TheGame.getTextures(), 1788, 698, 64, 79));
            nextGeneration.setVisible(false);
            showAll.setVisible(false);
            borderDown.setPosition(0, -80);
        } else {
            generationFolder.changeTextures(new TextureRegion(TheGame.getTextures(), 1724, 619, 64, 79), new TextureRegion(TheGame.getTextures(), 1724, 698, 64, 79));
            nextGeneration.setVisible(true);
            showAll.setVisible(true);
            borderDown.setPosition(0, 0);
        }
    }

    public static void buttonsFold() {
        if (menu.isVisible()) {
            buttonFolder.changeTextures(new TextureRegion(TheGame.getTextures(), 2411, 619, 48, 60), new TextureRegion(TheGame.getTextures(), 2411, 679, 48, 60));
            menu.setVisible(false);
            societyState.setVisible(false);
            borderUp.setPosition(0, Gdx.graphics.getHeight() + 80);
        } else {
            buttonFolder.changeTextures(new TextureRegion(TheGame.getTextures(), 2363, 619, 48, 60), new TextureRegion(TheGame.getTextures(), 2363, 679, 48, 60));
            menu.setVisible(true);
            societyState.setVisible(true);
            borderUp.setPosition(0, Gdx.graphics.getHeight());
        }
    }

    public static void zoomIn() {
        if ((Graph.getScaleX() > 0.009f)) {
            if (Graph.getY() < Gdx.graphics.getHeight() / 2f) {
                float tmp = (Gdx.graphics.getHeight() / 2f - Graph.getY());
                float tmp2 = Math.abs(Graph.getHeight() * Graph.getScaleY() - Graph.getHeight() * (Graph.getScaleY() - 0.1f * Graph.getScaleY() / 2f));
                Graph.setY(Graph.getY() + tmp2 * tmp / (Graph.getHeight() * Graph.getScaleY()));
            }
            Graph.setScale(Graph.getScaleX() - 0.1f * Graph.getScaleX() / 2f, Graph.getScaleY() - 0.1f * Graph.getScaleY() / 2f);

        }
    }

    public static void zoomOut() {
        if (Graph.getScaleY() < 12f) {
            if (Graph.getY() < Gdx.graphics.getHeight() / 2f) {
                float tmp = (Gdx.graphics.getHeight() / 2f - Graph.getY());
                float tmp2 = Math.abs(Graph.getHeight() * Graph.getScaleY() - Graph.getHeight() * (Graph.getScaleY() + 0.1f * Graph.getScaleY() / 2f));
                Graph.setY(Graph.getY() - tmp2 * tmp / (Graph.getHeight() * Graph.getScaleY()));
            }
            Graph.setScale(Graph.getScaleX() + 0.1f * Graph.getScaleY() / 2f, Graph.getScaleY() + 0.1f * Graph.getScaleY() / 2f);
        }
    }

    public static void dragGraph(float y, float touchY) {
        Graph.drag(y, touchY);
    }

    public static Music getBgm() {
        return bgm;
    }
    public static boolean isTransitionSkipped(){
        return next_generation_transition_skipped;
    }
    public static void goToFarming() {

        if(transition.getColor().a==0) {
            stage.addActor(transition);
            stage.addListener(new InputListener(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;

                }
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    next_generation_transition_skipped=true;
                    TheGame.getInstance().setFarming();
                }
            });
        }
        if(transition.getColor().a!=1) {
            Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    transition.setAlpha(transition.getColor().a+0.01f);
                    goToFarming();
                }

            }, 0.0001f);
        }
    }

    private void addListeners() {
        menu.addListener(new Menu_Listener());
        oneToOne.addListener(new Normal_Size_Listener());
        generationFolder.addListener(new Generation_Folder_Listener());
        buttonFolder.addListener(new Button_Folder_Listener());
        nextGeneration.addListener(new Next_Generation_Listener());
    }

    public void updateTextures() {
        oneToOne = new ButtonActor(new TextureRegion(TheGame.getTextures(), 1366, 619, 66, 116), new TextureRegion(TheGame.getTextures(), 1366, 735, 66, 116), Gdx.graphics.getWidth() - 66, Gdx.graphics.getHeight() / 2 - 58, 66, 116);
        nextGeneration = new ButtonActor(new TextureRegion(TheGame.getTextures(), 1432, 619, 292, 79), new TextureRegion(TheGame.getTextures(), 1432, 698, 292, 79), Gdx.graphics.getWidth() / 2f - 146, 10, 292, 79);
        nextGeneration.setVisible(false);
        showAll = new ButtonActor(new TextureRegion(TheGame.getTextures(), 1852, 619, 100, 79), new TextureRegion(TheGame.getTextures(), 1852, 698, 100, 79), 0, 10, 100, 79);
        showAll.setVisible(false);
        societyState = new ButtonActor(new TextureRegion(TheGame.getTextures(), 1952, 619, 311, 60), new TextureRegion(TheGame.getTextures(), 1952, 689, 311, 60), Gdx.graphics.getWidth() / 2f - 155, Gdx.graphics.getHeight() - 70, 311, 60);
        societyState.setVisible(false);
        menu = new ButtonActor(new TextureRegion(TheGame.getTextures(), 2263, 619, 100, 60), new TextureRegion(TheGame.getTextures(), 2263, 679, 100, 60), 0, Gdx.graphics.getHeight() - 70, 100, 60);
        menu.setVisible(false);
        generationFolder = new ButtonActor(new TextureRegion(TheGame.getTextures(), 1788, 619, 64, 79), new TextureRegion(TheGame.getTextures(), 1788, 698, 64, 79), Gdx.graphics.getWidth() - 64, 10, 64, 79);
        buttonFolder = new ButtonActor(new TextureRegion(TheGame.getTextures(), 2411, 619, 48, 60), new TextureRegion(TheGame.getTextures(), 2411, 679, 48, 60), Gdx.graphics.getWidth() - 48, Gdx.graphics.getHeight() - 70, 48, 60);
        addListeners();
        stage.addActor(oneToOne);
        stage.addActor(nextGeneration);
        stage.addActor(showAll);
        stage.addActor(societyState);
        stage.addActor(menu);
        stage.addActor(generationFolder);
        stage.addActor(buttonFolder);
    }

    @Override
    public void show() {
        InputMultiplexer multiplexer = new InputMultiplexer();
        Graph = new GraphActor(0, 160, Gdx.graphics.getWidth(), 100000);
        Graph.set(TheGame.getCurrentSociety().Farming, TheGame.getCurrentSociety().Production, TheGame.getCurrentSociety().Service);
        borderDown = new TextureActor(new TextureRegion(TheGame.getTextures(), 0, 619, 1366, 232), 0, -80, 1366, 232);
        borderUp = new TextureActor(new TextureRegion(TheGame.getTextures(), 0, 619, 1366, 232), 0, Gdx.graphics.getHeight() + 80, 1366, -232);
        transition=new TextureActor(new TextureRegion(TheGame.getTextures(), 0, 840, 1, 1),0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        transition.setColor(1,1,1,0);
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        stage.addActor(Graph);
        Graph.normalSize();
        stage.addActor(borderDown);
        stage.addActor(borderUp);
        updateTextures();
        TheGame.setBackgroundMusic(bgm);
        GestureDetector phone_gestures = new GestureDetector(new Phone_Graph_Listener());
        InputProcessor scrolls = new Desktop_Graph_Listener();
        multiplexer.addProcessor(0, stage);
        multiplexer.addProcessor(1, phone_gestures);
        multiplexer.addProcessor(2, scrolls);
        Gdx.input.setInputProcessor(multiplexer);
        stage.act(2);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void dispose() {

        stage.dispose();
    }
}
