package com.advancable.socrise.Model.Screens;

import com.advancable.socrise.TheGame;
import com.advancable.socrise.View.TextureActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * @author Somkin Ivan.
 *         Intro screen with "Advancable games" logo.
 */
public class Logo_Screen implements Screen {
    public static int bg;
    Texture Advancable;
    TextureActor Advancable_shadow;
    TextureActor Advancable_main;
    TextureActor Glow;
    TextureActor games;
    Music electricity;
    private Batch batch;
    boolean animation;
    private Stage stage;

    public Logo_Screen(SpriteBatch batch) {
       this.batch=batch;
    }

    private void backgroundLightener(final int m) {
        Timer timer3 = new Timer();
        timer3.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                if (m > 0) {
                    Logo_Screen.bg = Logo_Screen.bg + 1;
                    backgroundLightener(m - 1);
                }
            }
        }, 0.05f);
    }

    @Override
    public void show() {
        bg = 0;
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        Advancable = new Texture(Gdx.files.internal(TheGame.getAssetsPath() + "Advancable logo\\textures.png"));
        Advancable_shadow = new TextureActor(new TextureRegion(Advancable, 0, 0, 659, 122), (float) Gdx.graphics.getWidth() / 2f - 330 * (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * TheGame.getPpuX(), (float) Gdx.graphics.getHeight() / 2f-(TheGame.getAssetsPath().equals("") ? 0 : 1) *61 * TheGame.getPpuY(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 659 * TheGame.getPpuX(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 122 * TheGame.getPpuX());
        Advancable_main = new TextureActor(new TextureRegion(Advancable, 142, 122, 659, 122), (float) Gdx.graphics.getWidth() / 2f - 330* (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * TheGame.getPpuX(), (float) Gdx.graphics.getHeight() / 2f-(TheGame.getAssetsPath().equals("") ? 0 : 1) *61* TheGame.getPpuY(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 659 * TheGame.getPpuX(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 122 * TheGame.getPpuX());
        Glow = new TextureActor(new TextureRegion(Advancable, 0, 122, 142, 122), (float) Gdx.graphics.getWidth() / 2f - 330 * (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * TheGame.getPpuX(), (float) Gdx.graphics.getHeight() / 2f-(TheGame.getAssetsPath().equals("") ? 0 : 1) *61 * TheGame.getPpuY(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 142 * TheGame.getPpuX(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 122 * TheGame.getPpuX());
        games = new TextureActor(new TextureRegion(Advancable, 659, 0, 140, 35), (float) Gdx.graphics.getWidth() / 2f + 180 * (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * TheGame.getPpuX(), (float) Gdx.graphics.getHeight() / 2f - (TheGame.getAssetsPath().equals("") ? 0.2f : 1) *122 * TheGame.getPpuY(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 140 * TheGame.getPpuX(), (TheGame.getAssetsPath().equals("") ? 1.5f : 1) * 35 * TheGame.getPpuX());
        animation = true;
        stage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                animation = false;
                stage.clear();
                electricity.stop();

                Timer timer = new Timer();
                timer.scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        TheGame.getInstance().setMenu();
                    }

                }, 0.5f);
            }
        });
        stage.addActor(Advancable_shadow);
        electricity = Gdx.audio.newMusic(Gdx.files.internal(TheGame.getAssetsPath() + "Advancable logo\\turn on.mp3"));
        Timer timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                if (animation) {

                    electricity.play();
                    Timer timer = new Timer();
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            if (animation) {
                                stage.addActor(Glow);
                                backgroundLightener(44);
                                Timer timer = new Timer();
                                timer.scheduleTask(new Timer.Task() {
                                    @Override
                                    public void run() {
                                        if (animation) {
                                            stage.addActor(Advancable_main);
                                            Timer timer = new Timer();
                                            timer.scheduleTask(new Timer.Task() {
                                                @Override
                                                public void run() {
                                                    if (animation) {
                                                        stage.addActor(games);
                                                        Timer timer = new Timer();
                                                        timer.scheduleTask(
                                                                new Timer.Task()
                                                                {
                                                                    @Override
                                                                    public void run() {
                                                                        stage.clear();
                                                                        Timer timer = new Timer();
                                                                        timer.scheduleTask(new Timer.Task() {
                                                                            @Override
                                                                            public void run() {
                                                                                if (animation)
                                                                                    TheGame.getInstance().setMenu();

                                                                            }

                                                                        }, 1f);

                                                                    }

                                                                }, 4.7f);}
                                                }
                                            }, 3f);
                                        }
                                    }
                                }, 0.19f);

                            }
                        }
                    }, 0.33f);
                }
            }
        }, 1f);
        Gdx.input.setInputProcessor(stage);
        stage.act(2);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(bg / 255f, bg / 255f, bg / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}


