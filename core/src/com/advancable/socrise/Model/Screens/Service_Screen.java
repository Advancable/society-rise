package com.advancable.socrise.Model.Screens;

import com.advancable.socrise.Controller.ServiceScreen.Gain_Points_For_Service_Listener;
import com.advancable.socrise.TheGame;
import com.advancable.socrise.View.CircleActor;
import com.advancable.socrise.View.TextActor;
import com.advancable.socrise.View.TextureActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Locale;
/**
 * @author Somkin Ivan
 *         Screen where you gain points for Production.
 */
public class Service_Screen implements Screen {
    private static Stage stage;
    private static TextActor PointCounter;
    private static CircleActor pointer;
    private static int i = 0;
    private Batch batch;
    private Music bgm;
    private TextureActor SERVE;
    private TextActor CountDownToStart;
    private TextActor YearsToServeLeft;
    private TextureActor transition;
    private int yearsPassed;
    private TextActor ServicemenCounter;
    int n;
    private TextureActor totalScoreTransition;
    private TextActor TotalScore;

    public Service_Screen(SpriteBatch batch) {
        this.batch = batch;
        bgm = Gdx.audio.newMusic(Gdx.files.internal(TheGame.getAssetsPath() + "music\\service\\1.mp3"));
    }

    public static void movePointer(float x, float y) {
        pointer.setPosition(x, y);
    }

    public static void getPoints(float screenX, float screenY) {
            i++;
            if (i == 10) {
                i = 0;
                TheGame.getCurrentSociety().addPointsToService();
                updatePointCounter();
            }
            movePointer(screenX - 20, Gdx.graphics.getHeight() - screenY - 20);
    }

    public static void updatePointCounter(){
        PointCounter.changeText(String.format(Locale.ENGLISH, "%4d", TheGame.getCurrentSociety().ServiceTurns[TheGame.getCurrentSociety().NumberOfTurns]));
    }

    private void animation_passTransition() {
        if (transition.getColor().a == 1) {
            stage.addActor(transition);
        }
        if (transition.getColor().a != 0) {
            Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    transition.setAlpha(transition.getColor().a - 0.01f);
                    animation_passTransition();
                }

            }, 0.0001f);
        }
    }
    private void goToTransition(){
        if (transition.getColor().r != 1) {
            Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    transition.setColor(transition.getColor().r + 0.01f,transition.getColor().g + 0.01f,transition.getColor().b + 0.01f,1);
                    goToTransition();
                }

            }, 0.001f);
        }
        if(transition.getColor().r == 1){
            countPoints();
            TheGame.getCurrentSociety().NumberOfTurns++;
            TheGame.getInstance().setGraph();

        }
    }
    private void animation_showTotalScore(){
        if (totalScoreTransition.getColor().a != 0) {
            Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    totalScoreTransition.setAlpha(totalScoreTransition.getColor().a - 0.01f);
                    animation_showTotalScore();
                }

            }, 0.0001f);
        }
        if(totalScoreTransition.getColor().a == 0){
            Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    goToTransition();
                }

            },1f);
        }
    }

    private void addOneYearToCounter() {
        YearsToServeLeft.changeText(String.format(Locale.ENGLISH, "%d:%s%d", yearsPassed / 60, yearsPassed % 60 < 10 ? "0" : "", yearsPassed % 60));
    }

    private void countPoints() {
        TheGame.getCurrentSociety().ServiceTurns[TheGame.getCurrentSociety().NumberOfTurns] *= TheGame.getCurrentSociety().Population * 1f / 111f;
        TheGame.getCurrentSociety().Service += TheGame.getCurrentSociety().ServiceTurns[TheGame.getCurrentSociety().NumberOfTurns];
    }
    private void animation_showPopulationMultiplier(){
        if (n != TheGame.getCurrentSociety().Population / 111f) {
            final Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    n+=(int)(TheGame.getCurrentSociety().Population / 111f);
                    ServicemenCounter.changeText(String.format(Locale.UK,"x  %d",n));
                    animation_showPopulationMultiplier();
                }
            }, 0.001f);
        }else{
            TotalScore.changeText(String.format(Locale.ENGLISH,"=%10d",(int)(TheGame.getCurrentSociety().Population / 111f*TheGame.getCurrentSociety().ServiceTurns[TheGame.getCurrentSociety().NumberOfTurns])));
            animation_showTotalScore();
        }
    }
    private void animation_movePointCounterToTheLeft() {
        if (PointCounter.getX()>Gdx.graphics.getWidth()/2f-200) {
            final Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    PointCounter.setX(PointCounter.getX()-2);
                    animation_movePointCounterToTheLeft();
                }
            }, 0.0001f);
        }else{
            final Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    stage.addActor(ServicemenCounter);
                    animation_showPopulationMultiplier();
                }
            }, 1f);
        }
    }

    private void animation_startLife() {
        final Timer life = new Timer();
        life.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                if (yearsPassed < TheGame.getCurrentSociety().LifeSpan) {
                    yearsPassed++;
                    addOneYearToCounter();
                } else {
                    Gdx.input.setInputProcessor(null);
                    life.stop();
                    life.clear();
                    TheGame.setBackgroundMusic(null);
                    stage.clear();
                    transition.setColor(0, 0, 0, 1);
                    stage.addActor(transition);
                    stage.addActor(PointCounter);
                    stage.addActor(TotalScore);
                    stage.addActor(totalScoreTransition);
                    n = 0;
                    animation_movePointCounterToTheLeft();

                }
            }
        }, 1f, 1f);
    }

    private void animation_startCountdown() {
        Timer timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                stage.addActor(CountDownToStart);
                Timer timer = new Timer();
                timer.scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        CountDownToStart.changeText("2");
                        Timer timer = new Timer();
                        timer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                CountDownToStart.changeText("1");
                                Timer timer = new Timer();
                                timer.scheduleTask(new Timer.Task() {
                                    @Override
                                    public void run() {
                                        CountDownToStart.changeText("");
                                        stage.addActor(SERVE);
                                        stage.addActor(PointCounter);
                                        stage.addActor(YearsToServeLeft);
                                        stage.addActor(TotalScore);
                                        Gdx.input.setInputProcessor(new Gain_Points_For_Service_Listener());
                                        animation_startLife();
                                    }

                                }, 1f);
                            }

                        }, 1f);
                    }

                }, 1f);
            }

        }, 1f);
    }
    private void do_actors(){
        SERVE = new TextureActor(new TextureRegion(TheGame.getTextures(), 2045,124, 456, 57), Gdx.graphics.getWidth() / 2f - 230, Gdx.graphics.getHeight()-200, 456, 57);
        pointer = new CircleActor(-100, -100, 20, 1, 1, 1, 1, 0, 1, 1, 1, 1);
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.color = Color.WHITE;
        parameter.size = 60;
        BitmapFont font = TheGame.get_Cantarell_Bold_Generator().generateFont(parameter);
        transition = new TextureActor(new TextureRegion(TheGame.getTextures(), 0, 840, 1, 1), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        transition.setColor(1, 1, 1, 1);
        totalScoreTransition = new TextureActor(new TextureRegion(TheGame.getTextures(), 0, 840, 1, 1), Gdx.graphics.getWidth() / 2f-200, Gdx.graphics.getHeight() / 2f-50, 400, 100);
        totalScoreTransition.setColor(0, 0, 0, 1);
        PointCounter = new TextActor(font, String.format(Locale.ENGLISH, "%4d", TheGame.getCurrentSociety().ServiceTurns[TheGame.getCurrentSociety().NumberOfTurns]), Gdx.graphics.getWidth() / 2f - 74, Gdx.graphics.getHeight() / 5f * 3 + 22);
        ServicemenCounter = new TextActor(font, "x  0", Gdx.graphics.getWidth() / 2f-20, Gdx.graphics.getHeight() / 5f * 3 + 22);
        yearsPassed = 0;
        YearsToServeLeft = new TextActor(font, "0:00", Gdx.graphics.getWidth() / 2f - 70, 60);
        parameter.size = 60;
        font = TheGame.get_Cantarell_Bold_Generator().generateFont(parameter);
        CountDownToStart = new TextActor(font, "3", Gdx.graphics.getWidth() / 2f - 20, Gdx.graphics.getHeight() / 5f * 3 + 20);
        TotalScore = new TextActor(font, "", Gdx.graphics.getWidth() / 2f-200, Gdx.graphics.getHeight() / 2f+30);
        stage.addActor(transition);
        stage.addActor(pointer);
    }
    @Override
    public void show() {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
       do_actors();
        animation_startCountdown();
        animation_passTransition();
        TheGame.setBackgroundMusic(bgm);


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(73f/255f,101f/255f,1,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
