package com.advancable.socrise.Model.Screens;



import com.advancable.socrise.View.LineActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * @author Somkin Ivan
 *         It's a main menu.
 */
public class Loading_Screen implements Screen {
    LineActor[] lines;
    private Stage stage;
    private Batch batch;

    public Loading_Screen(SpriteBatch batch) {
        this.batch=batch;

    }


    @Override
    public void show() {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        lines = new LineActor[15];
        for (int i = 0; i < 15; i++) {
            lines[i] = new LineActor(0,(Gdx.graphics.getHeight() / 15) * i, Gdx.graphics.getWidth(), (Gdx.graphics.getHeight() / 15) * i, 0.5f, 0.5f, 0.5f, 1,1);
            stage.addActor(lines[i]);
        }
        Gdx.input.setInputProcessor(stage);
        stage.act(2);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
            stage.dispose();

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}

