package com.advancable.socrise.Model.Screens;

import com.advancable.socrise.Controller.Menu_Listener;
import com.advancable.socrise.TheGame;
import com.advancable.socrise.View.ButtonActor;
import com.advancable.socrise.View.LineActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * @author Somkin Ivan.
 * Screen with credits and guide.
 */
public class Help_Screen implements Screen{
    private Stage stage;
    ButtonActor tips;
    ButtonActor credits;
    LineActor[] lines;
    ButtonActor back;
    private Batch batch;
    private void addListeners(){
        tips.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.getTarget().act(1);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (event.getTarget() == event.getListenerActor().hit(x, y, true)) {
                    Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s", TheGame.getAssetsPath(),"sound\\menu button.mp3")));
                    sound.play(TheGame.volume);
                    Timer timer = new Timer();
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            tips.setVisible(false);
                            credits.setVisible(false);
                        }

                    }, 0.1f);
                }else{
                    event.getTarget().act(2);
                }

            }
        });
        credits.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.getTarget().act(1);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (event.getTarget() == event.getListenerActor().hit(x, y, true)) {
                    Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s", TheGame.getAssetsPath(),"sound\\menu button.mp3")));
                    sound.play(TheGame.volume);
                    Timer timer = new Timer();
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            tips.setVisible(false);
                            credits.setVisible(false);
                        }

                    }, 0.1f);
                }else{
                    event.getTarget().act(2);
                }

            }
        });
        back.addListener(new Menu_Listener());
    }
    public void updateTextures(){
        stage.clear();
        tips= new ButtonActor(new TextureRegion(TheGame.getTextures(), 1595, 0, 450, 152), new TextureRegion(TheGame.getTextures(), 1595, 302, 450, 152), Gdx.graphics.getWidth() / 2f - 225, Gdx.graphics.getHeight() / 2f, 450, 151);
        credits = new ButtonActor(new TextureRegion(TheGame.getTextures(), 1595, 153, 450, 152), new TextureRegion(TheGame.getTextures(), 1595, 453, 450, 152), Gdx.graphics.getWidth() / 2f - 225, Gdx.graphics.getHeight() / 2f - 180, 450, 151);
        back = new ButtonActor(new TextureRegion(TheGame.getTextures(), 0, 225, 308, 102), new TextureRegion(TheGame.getTextures(), 0, 327, 308, 102), Gdx.graphics.getWidth() / 2f - 154, 0, 308, 102);
        for (int i = 0; i < 15; i++) {
            lines[i] = new LineActor(0,Gdx.graphics.getHeight()+ (Gdx.graphics.getHeight() / 15f) * i, Gdx.graphics.getWidth(), 0, 1, 1, 1, 1,1);
            stage.addActor(lines[i]);
        }
        addListeners();
        stage.addActor(back);
        stage.addActor(tips);
        stage.addActor(credits);
    }
    public Help_Screen(SpriteBatch batch) {
        this.batch=batch;
    }

    @Override
    public void show() {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, batch);
        lines = new LineActor[15];
        updateTextures();
        Gdx.input.setInputProcessor(stage);
        stage.act(2);
    }

    @Override
    public void render(float delta) {
        for (int i = 0; i < 15; i++) {
            if (lines[i].getY() > 0) {
                lines[i].setPosition(0,lines[i].getY() - 3);
                lines[i].setSize(Gdx.graphics.getWidth(),0);
            } else {
                lines[i].setY(Gdx.graphics.getHeight());
                lines[i].setHeight(Gdx.graphics.getHeight());
            }
        }
        Gdx.gl.glClearColor(73f/255f,101f/255f,1,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
