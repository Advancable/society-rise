package com.advancable.socrise.Model;

/**
 * @author Somkin Ivan
 * Result of your game.
 * Contains Productivity, Management and Science levels, turn number, and all the turns result.
 */
public class Society {
    public long Farming;
    public long Production;
    public long Service;
    public long[] FarmingTurns;
    public long[] ProductionTurns;
    public long[] ServiceTurns;
    public long Population;
    public int LifeSpan;
    public int NumberOfTurns;
    public Society(long Farming,long Production,long Service,long[] FarmingTurns,long[] ProductionTurns,long[] ServiceTurns,long Population,int LifeSpan,int NumberOfTurns){
        this.Farming=Farming;
        this.Production= Production;
        this.Service=Service;
        this.FarmingTurns =FarmingTurns;
        this.ProductionTurns=ProductionTurns;
        this.ServiceTurns=ServiceTurns;
        this.Population=Population;
        this.LifeSpan=LifeSpan;
        this.NumberOfTurns=NumberOfTurns;
    }
    public void addPointsToFarming(){
        this.FarmingTurns[this.NumberOfTurns]+=1;
    }
    public void addPointsToProduction(){
        this.ProductionTurns[this.NumberOfTurns]+=10;
    }
    public void addPointsToService(){
        this.ServiceTurns[this.NumberOfTurns]+=100;
    }
}
