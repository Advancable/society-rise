package com.advancable.socrise;


import com.advancable.socrise.Model.Screens.*;
import com.advancable.socrise.Model.Society;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Main game class.
 */
public class TheGame extends Game {
    private static Logo_Screen logoScreen;
    private static Menu_Screen menuScreen;
    private static Play_Screen playScreen;
    private static Graph_Screen graphScreen;
    private static Options_Screen optionsScreen;
    private static Help_Screen helpScreen;
    private static Music bgm;
    private static float ppuX, ppuY;
    private static FreeTypeFontGenerator Cantarell_Bold_Generator;
    private static FreeTypeFontGenerator Cantarell_Regular_Generator;
    private static ShapeRenderer shape;
    private static SpriteBatch batch;
    private static String language;
    private static TheGame ourInstance = new TheGame();
    private static String assetsPath = "";
    private static Texture textures;
    private static Society currentSociety;
    private static Farming_Screen farmingScreen;
    private static Production_Screen productionScreen;
    private static Service_Screen serviceScreen;
    public static float volume;
    private TheGame() {
    }

    public static TheGame getInstance() {
        return ourInstance;
    }

    public static void stopBackgroundMusic() {
        if (TheGame.bgm != null)
            TheGame.bgm.stop();
    }
    public static void changeVolume(float volume){
        TheGame.bgm.setVolume(volume);
        TheGame.volume=volume;
    }
    public static void setBackgroundMusic(Music bgm) {
        if (TheGame.bgm != null)
            TheGame.bgm.stop();
        TheGame.bgm = bgm;
        if (bgm != null) {
            TheGame.bgm.setLooping(true);
            TheGame.bgm.play();
            TheGame.bgm.setVolume(volume);
        }
    }

    public static String getAssetsPath() {
        return assetsPath;
    }

    public static void setAssetsPath(String assetsPath) {
        TheGame.assetsPath = assetsPath;
    }

    public static Texture getTextures() {
        return textures;
    }

    public static FreeTypeFontGenerator get_Cantarell_Bold_Generator() {
        return Cantarell_Bold_Generator;
    }

    public static FreeTypeFontGenerator get_Cantarell_Regular_Generator() {
        return Cantarell_Regular_Generator;
    }

    public static float getPpuY() {
        return ppuY;
    }

    public static float getPpuX() {
        return ppuX;
    }

    public static String getLanguage() {
        return TheGame.language;
    }

    public static void setLanguage(String language) {
        TheGame.language = language;
    }

    public static ShapeRenderer getShape() {
        return shape;
    }

    public static SpriteBatch getBatch() {
        return batch;
    }

    public static Society getCurrentSociety() {
        return currentSociety;
    }

    public static void updateTexturesLanguage() {
        textures = new Texture(String.format("%s%s%s%s", TheGame.getAssetsPath(), "textures\\", TheGame.language, "\\textures.png"));
    }


    public static void startNewSociety() {
        currentSociety = new Society(0, 0, 0, new long[31], new long[31], new long[31], 111, 30, 0);
        for (int i = 0; i < 31; i++) {
            currentSociety.ProductionTurns[i] = 0;
            currentSociety.FarmingTurns[i] = 0;
            currentSociety.ServiceTurns[i] = 0;
        }
    }

    @Override
    public void create() {
        Cantarell_Bold_Generator = new FreeTypeFontGenerator(Gdx.files.internal(TheGame.getAssetsPath() + "fonts\\Cantarell-Bold.ttf"));
        Cantarell_Regular_Generator = new FreeTypeFontGenerator(Gdx.files.internal(TheGame.getAssetsPath() + "fonts\\Cantarell-Regular.ttf"));
        batch = new SpriteBatch();
        shape = new ShapeRenderer();
        ppuX = (float) Gdx.graphics.getWidth() / 1366f;
        ppuY = (float) Gdx.graphics.getHeight() / 768f;
        setLanguage("eng");
        textures = new Texture(String.format("%s%s%s%s", TheGame.getAssetsPath(), "textures\\", TheGame.language, "\\textures.png"));
        logoScreen = new Logo_Screen(batch);
        menuScreen = new Menu_Screen(batch);
        playScreen = new Play_Screen(batch);
        graphScreen = new Graph_Screen(batch);
        optionsScreen = new Options_Screen(batch);
        helpScreen = new Help_Screen(batch);
        farmingScreen = new Farming_Screen(batch);
        productionScreen = new Production_Screen(batch);
        serviceScreen = new Service_Screen(batch);
        volume=0.5f;
        setLogo();
    }

    public void setLogo() {
        setScreen(logoScreen);
    }

    public void setMenu() {

        setScreen(menuScreen);
        if (bgm != Menu_Screen.getBgm())
            TheGame.setBackgroundMusic(Menu_Screen.getBgm());

    }

    public void setPlay() {
        setScreen(playScreen);
    }

    public void setGraph() {
        setScreen(graphScreen);
        if (bgm != Graph_Screen.getBgm())
            TheGame.setBackgroundMusic(Graph_Screen.getBgm());

    }

    public void setOptions() {
        setScreen(optionsScreen);
    }

    public void setHelp() {
        setScreen(helpScreen);
    }

    public void setProduction() {
        setScreen(productionScreen);
    }

    public void setFarming() {
        setScreen(farmingScreen);
    }

    public void setService() {
        setScreen(serviceScreen);
    }
}


