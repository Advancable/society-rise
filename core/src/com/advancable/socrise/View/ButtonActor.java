package com.advancable.socrise.View;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Actor with 2 changing textures.
 */
public class ButtonActor extends Actor {
    private TextureRegion img,img2;
    public boolean toggled;
    public void changeTextures(TextureRegion img,TextureRegion img2){
        this.img=img;
        this.img2=img2;
    }
    @Override
    public void draw(Batch batch, float parentAlpha){
        if (!toggled)
            batch.draw(img,getX(),getY(),getWidth(),getHeight());
        else
            batch.draw(img2,getX(),getY(),getWidth(),getHeight());
    }
    @Override
    public void act(float delta){
        toggled = delta == 1;
    }
    public ButtonActor(TextureRegion img, TextureRegion img2, float x, float y, float width, float height){
        this.img=img;
        this.img2=img2;
        setPosition(x,y);
        setSize(width,height);
    }
}