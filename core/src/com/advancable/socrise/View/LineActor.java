package com.advancable.socrise.View;
import com.advancable.socrise.TheGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;


/**
 * Just a line.
 */
public class LineActor extends Actor{
    public Color color;
    public float size;
    @Override
    public void draw(Batch batch,float parentAlpha){
        batch.end();
        ShapeRenderer shape= TheGame.getShape();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(color);
        shape.rect(getX(),getY(),getX(),getY(), (float)Math.sqrt((double)(getWidth()*getWidth()+getHeight()*getHeight())),size/getScaleX()<1?0:size/getScaleX(),getScaleX(),getScaleY(), (float)Math.toDegrees((float) Math.asin((double)( getHeight()/ (float)(Math.sqrt((double)((getWidth()*getWidth()+getHeight()*getHeight())))   )))));
        shape.end();
        batch.begin();
    }
    public LineActor(float x, float y, float width, float height, float r, float g, float b, float a,float size){
        setPosition(x,y);
        setSize(width, height);
        setScale(1,1);
        color = new Color(r,g,b,a);
        this.size=size;
    }

}