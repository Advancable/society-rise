package com.advancable.socrise.View;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class TextActor extends Actor {
    public String str;
    public BitmapFont font;
    public TextActor(BitmapFont font,String str,float x,float y){
        this.font=font;
        this.str=str;
        setPosition(x,y);
    }
    public void changeText(String str){
        this.str=str;
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        font.draw(batch,str,getX(),getY());
    }
    public String getStr(){
        return str;
    }
}
