package com.advancable.socrise.View;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Actor with texture. Simple.
 */
public class TextureActor extends Actor {
    public TextureRegion img;
    public void setAlpha(float a){
        setColor(getColor().r,getColor().g,getColor().b,a);
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.enableBlending();
        Color tmp=batch.getColor();
        batch.setColor(getColor());
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
        batch.setColor(tmp);
    }

    public TextureActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x, y );
        setSize(width, height);
    }

}
