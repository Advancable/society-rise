package com.advancable.socrise.View;

import com.advancable.socrise.TheGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Timer;

import java.util.Locale;

/**
 * @author Somkin Ivan
 * Graph with Farming, Productivity and Service.
 */
public class GraphActor extends Actor {
    private long Farming;
    private long Productivity;
    private long Service;
    private int velocity;
    private BitmapFont numberFont1;
    private BitmapFont numberFont10;
    private BitmapFont numberFont100;
    private BitmapFont numberFont1000;
    private int getNumberOfDigits(long a){
        int s=0;
        if(a!=0) {
            while (a > 0) {
                a /= 10;
                s++;
            }
        }else
        s=1;
        return s;
    }
    public void drag(float y, float touchY){
        setVelocity(0);
        if (((getY()> (-getHeight() *getScaleY() + Gdx.graphics.getHeight() / 2f)) && (getY() < Gdx.graphics.getHeight() / 2f)) && (getY() - touchY + y > (-getHeight() *getScaleY() + Gdx.graphics.getHeight() / 2f)) && (getY() - touchY + y < Gdx.graphics.getHeight() / 2f))
            setPosition(getX(), getY() - touchY + y);
        if (((getY() <= (-getHeight() * getScaleY() + Gdx.graphics.getHeight() / 2f)) || (getY() >= Gdx.graphics.getHeight() / 2f)))
            setPosition(getX(), getY() - touchY + y);

    }
    public void setVelocity(int velocity){
        this.velocity=velocity;
    }
    public void accelerate() {
        if(velocity!=0) {
            Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    setY(getY() + velocity);

                    velocity -= Math.abs(velocity) >= 1 ? (velocity / Math.abs(velocity)) : velocity;
                    if (getY() >= Gdx.graphics.getHeight() / 2f) {
                        velocity = 0;
                        setY(Gdx.graphics.getHeight() / 2f);
                    }
                    if (getY() + getHeight() * getScaleY() <= Gdx.graphics.getHeight() / 2f) {
                        velocity = 0;
                        setY(Gdx.graphics.getHeight() / 2f - getHeight()*getScaleY());
                    }
                    accelerate();
                }

            }, 0.01f);
        }
    }
    private void drawThousands(Batch batch,ShapeRenderer shape,int n,int boldness){
        for (int i = Math.round((80-getY())/getScaleY()/(5*n)-1>0?Math.round((80-getY())/getScaleY()/(5*n))-1:0); i < 40+Math.round((80-getY())/getScaleY()/(5*n))-1; i++)
            if((getY()+ i*(5*n)*getScaleY()<Gdx.graphics.getHeight())&((getY()+ i*(5*n)*getScaleY()>=0))&(i*(5*n)*getScaleY()<=getHeight()*getScaleY()))
                if (n/10f * getScaleY() > 0.45) {
                    shape.setColor(Color.BLACK);
                    shape.rect(0, (getY() + i * (5 * n) * getScaleY()), Gdx.graphics.getWidth(), boldness);
                    shape.end();
                    batch.begin();
                    if((n==1&&i!=0&&(i*n/10)!=(i*n/10f)))
                        numberFont1.draw(batch,String.format(Locale.UK,"%8d",i*1000*n), getX(), (getY() + i * (5 * n) * getScaleY())+15);
                    if((n==10&&i!=0&&(i*n/100)!=(i*n/100f)))
                    numberFont1.draw(batch,String.format(Locale.UK,"%8d",i*1000*n), getX(), (getY() + i * (5 * n) * getScaleY())+15);
                    if(n==100&&i!=0&&(i*n/1000)!=(i*n/1000f))
                        numberFont10.draw(batch, String.format(Locale.UK,"%8d",i*1000*n), getX(), (getY() + i * (5 * n) * getScaleY())+15);
                    if(n==1000&&i!=0&&(i*n/10000)!=(i*n/10000f))
                        numberFont100.draw(batch, String.format(Locale.UK,"%8d",i*1000*n), getX(), (getY() + i * (5 * n) * getScaleY())+17);
                    if(n==10000&&i!=0&&(i*n/100000)!=(i*n/100000f))
                        numberFont1000.draw(batch, String.format(Locale.UK,"%8d",i*1000*n), getX(), (getY() + i * (5 * n) * getScaleY())+19);

                    batch.end();
                    shape.begin(ShapeRenderer.ShapeType.Filled);
                }


    }
    public void set(long Productivity,long Management,long Science){
        this.Farming =Productivity;
        this.Productivity =Management;
        this.Service =Science;
    }
    public void normalSize(){
        long tmp= Math.max(Math.max(Farming, Productivity), Service);
        this.setPosition(0,160);
        if(TheGame.getCurrentSociety().Production!=0||TheGame.getCurrentSociety().Farming!=0||TheGame.getCurrentSociety().Service!=0)
            setScale(Gdx.graphics.getHeight()*100f/tmp,Gdx.graphics.getHeight()*100f/tmp);
        else
            setScale(12,12);
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();
        ShapeRenderer shape = TheGame.getShape();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(new Color(200f/255f,1f,200f/255f,1f));
        shape.rect(getX()+getWidth()/4f-22, getY(),45,9999);
        shape.setColor(new Color(1,187f/255f,187f/255f,1f));
        shape.rect(getX()+getWidth()/2f-22, getY(),45,9999);
        shape.setColor(new Color(179f/255f,190f/255f,1f,1f));
        shape.rect(getX()+getWidth()/4f*3-22, getY(),45,9999);
        shape.setColor(new Color(0f,200f/255f,0f,1f));
        shape.rect(getX()+getWidth()/4f-22, getY(),45, (Farming /10000f*50f)*getScaleY()-10>2?(Farming /10000f*50f)*getScaleY()-10:2);
        shape.triangle(getX()+getWidth()/4f-22,(Farming /10000f*50f)*getScaleY()>=10?getY()+(Farming /10000f*50f)*getScaleY()-10:getY(),getX()+getWidth()/4f,getY()+(Farming /10000f*50f)*getScaleY(),getX()+getWidth()/4f+23,(Farming /10000f*50f)*getScaleY()>=10?getY()+(Farming /10000f*50f)*getScaleY()-10:getY());
        shape.setColor(new Color(1f,48f/255f,48f/255f,1f));
        shape.rect(getX()+getWidth()/2f-22, getY(),45, (Productivity /10000f*50f)*getScaleY()-10>2?(Productivity /10000f*50f)*getScaleY()-10:2);
        shape.triangle(getX()+getWidth()/2f-22,(Productivity /10000f*50f)*getScaleY()>=10?getY()+(Productivity /10000f*50f)*getScaleY()-10:getY(),getX()+getWidth()/2f,getY()+(Productivity /10000f*50f)*getScaleY(),getX()+getWidth()/2f+23,(Productivity /10000f*50f)*getScaleY()>=10?getY()+(Productivity /10000f*50f)*getScaleY()-10:getY());
        shape.setColor(new Color(73f/255f,101f/255f,1f,1f));
        shape.rect(getX()+getWidth()/4f*3-22, getY(),45, (Service /10000f*50f)*getScaleY()-10>2?(Service /10000f*50f)*getScaleY()-10:2);
        shape.triangle(getX()+getWidth()/4f*3-22,(Service /10000f*50f)*getScaleY()>=10?getY()+(Service /10000f*50f)*getScaleY()-10:getY(),getX()+getWidth()/4f*3f,getY()+(Service /10000f*50f)*getScaleY(),getX()+getWidth()/4f*3f+23,(Service /10000f*50f)*getScaleY()>=10?getY()+(Service /10000f*50f)*getScaleY()-10:getY());
        shape.setColor(Color.BLACK);
        drawThousands(batch,shape,1,1);
        drawThousands(batch,shape,10,2);
        drawThousands(batch,shape,100,3);
        drawThousands(batch,shape,1000,4);
        drawThousands(batch,shape,10000,5);
        drawThousands(batch,shape,100000,6);
        shape.end();
        batch.begin();
        numberFont1000.draw(batch, String.format(Locale.UK,"%d", Farming), getWidth()/4f-6*getNumberOfDigits(Farming), (getY()-15));
        numberFont1000.draw(batch, String.format(Locale.UK,"%d", Productivity), getWidth()/2f-6*getNumberOfDigits(Productivity), (getY()-15));
        numberFont1000.draw(batch, String.format(Locale.UK,"%d", Service), getWidth()/4f*3-6*getNumberOfDigits(Service), (getY()-15));
    }
    public GraphActor(float x, float y, float width, float height){
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.color=Color.BLACK;
        parameter.size=15;
        numberFont1 = TheGame.get_Cantarell_Regular_Generator().generateFont(parameter);
        numberFont1.setColor(Color.BLACK);
        parameter.size=15;
        numberFont10 = TheGame.get_Cantarell_Bold_Generator().generateFont(parameter);
        numberFont10.setColor(Color.BLACK);
        parameter.size=17;
        numberFont100 = TheGame.get_Cantarell_Regular_Generator().generateFont(parameter);
        numberFont100.setColor(Color.BLACK);
        parameter.size=17;
        numberFont1000 = TheGame.get_Cantarell_Bold_Generator().generateFont(parameter);
        numberFont1000.setColor(Color.BLACK);
        setPosition(x,y);
        setSize(width, height);
        setScale(1f,1f);
    }

}
