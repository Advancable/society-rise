package com.advancable.socrise.View;

import com.advancable.socrise.TheGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author Somkin Ivan
 * Actor that makes a circle with a border.
 */
public class CircleActor extends Actor {
    float borderSize;
    Color borderColor;
    @Override
    public void draw(Batch batch, float parentAlpha){
        batch.end();
        ShapeRenderer shape= TheGame.getShape();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.setTransformMatrix(batch.getTransformMatrix());
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(borderColor);
        shape.ellipse(getX()-borderSize,getY()-borderSize,getWidth()+borderSize*2,getHeight()+borderSize*2);
        shape.setColor(getColor());
        shape.ellipse(getX(),getY(),getWidth(),getHeight());
        shape.end();
        batch.begin();
    }
    public CircleActor(float x, float y, float radius, float r, float g, float b, float a,float borderSize, float borderR, float borderG, float borderB, float borderA){
        setPosition(x-radius,y-radius);
        setSize(radius*2, radius*2);
        setColor(r,g,b,a);
        this.borderSize=borderSize;
        borderColor=new Color(borderR,borderG,borderB,borderA);
    }
}
