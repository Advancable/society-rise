package com.advancable.socrise.Controller.OptionsScreen;

import com.advancable.socrise.Model.Screens.Options_Screen;
import com.advancable.socrise.TheGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

/**
 * @author Somkin Ivan
 *         Listener that changes the sound while moving the volumeSlider.
 */
public class Volume_Changer implements InputProcessor {
    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if((screenX-25>=Gdx.graphics.getWidth() / 2f - 200)&&( screenX-25<=Gdx.graphics.getWidth() / 2f +150)&&(Gdx.graphics.getHeight()-screenY>= Gdx.graphics.getHeight() / 2f -13)&&(Gdx.graphics.getHeight()-screenY<= Gdx.graphics.getHeight() / 2f +67))
        Options_Screen.touchDownSlider(screenX-25);
        TheGame.changeVolume((screenX-25-Gdx.graphics.getWidth() / 2f + 175) /300f);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if((screenX<=Gdx.graphics.getWidth()/2f+15)&&(screenX>=Gdx.graphics.getWidth()/2f-15)) {
            Options_Screen.dragSlider(Gdx.graphics.getWidth()/2f-25);
            TheGame.changeVolume(0.5f);
        }
        else
        if((screenX-25>=Gdx.graphics.getWidth() / 2f - 175)&&( screenX-25<=Gdx.graphics.getWidth() / 2f +125)) {
            Options_Screen.dragSlider(screenX-25);
            TheGame.changeVolume((screenX-Gdx.graphics.getWidth() / 2f + 175) /300f);
        }
        else
        if(screenX-25<Gdx.graphics.getWidth() / 2f - 175) {
            TheGame.changeVolume(0);
            Options_Screen.dragSlider(Gdx.graphics.getWidth() / 2f - 175);
        }
            else {
            Options_Screen.dragSlider(Gdx.graphics.getWidth() / 2f + 125);
            TheGame.changeVolume(1);
        }
        Options_Screen.touchUpSlider();
        return true;

    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Options_Screen.dragSlider(screenX-25);
        TheGame.changeVolume((screenX-25-Gdx.graphics.getWidth() / 2f + 175) /300f);

        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
