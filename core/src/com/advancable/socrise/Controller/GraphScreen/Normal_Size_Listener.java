package com.advancable.socrise.Controller.GraphScreen;

import com.advancable.socrise.Model.Screens.Graph_Screen;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

/**
 * @author Somkin Ivan
 * Listener that makes graph on Graph_Screen a standard size.
 */
public class Normal_Size_Listener extends InputListener {
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        event.getTarget().act(1);
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (event.getTarget() == event.getListenerActor().hit(x, y, true)) {
            Graph_Screen.normalSize();
        }
        event.getTarget().act(2);
    }
}
