package com.advancable.socrise.Controller.GraphScreen;

import com.advancable.socrise.Model.Screens.Graph_Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;


/**
 * @author Somkin Ivan
 *         Listener that organises the income of points by panning.
 */
public class Phone_Graph_Listener implements GestureDetector.GestureListener {
    private float startY, zoomingStage;
    private boolean enablePan;


    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        startY = Gdx.graphics.getHeight() - y;
        enablePan = true;
        Graph_Screen.dragGraph(Gdx.graphics.getHeight() - y, startY);
        return true;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
            Graph_Screen.accelerateGraph((int)-velocityY/50);
        return true;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        if (enablePan) {
            Graph_Screen.dragGraph(Gdx.graphics.getHeight() - y, startY);
            startY +=Gdx.graphics.getHeight() - y- startY;
        }
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return true;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        enablePan = false;
        if (zoomingStage < 0)
            zoomingStage = initialDistance;
        if (zoomingStage - distance < -20) {
            zoomingStage = distance;
            Graph_Screen.zoomOut();
        }
        if (zoomingStage - distance > 20) {
            Graph_Screen.zoomIn();
            zoomingStage = distance;
        }

        return true;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }
}
