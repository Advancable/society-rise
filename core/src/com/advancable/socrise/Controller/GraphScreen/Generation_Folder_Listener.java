package com.advancable.socrise.Controller.GraphScreen;

import com.advancable.socrise.Model.Screens.Graph_Screen;
import com.advancable.socrise.TheGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Timer;

/**
 * @author Somkin Ivan
 *         Listener that folds generation buttons.
 */
public class Generation_Folder_Listener extends InputListener {
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        event.getTarget().act(1);
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (event.getTarget() == event.getListenerActor().hit(x, y, true)) {
            final Sound sound = Gdx.audio.newSound(Gdx.files.internal(String.format("%s%s", TheGame.getAssetsPath(), "sound\\menu button.mp3")));
            sound.play(TheGame.volume);
            final Timer timer = new Timer();
            timer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    sound.dispose();
                }

            }, 1f);
            Graph_Screen.generationFold();
        }
        event.getTarget().act(2);

    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (event.getTarget() == event.getListenerActor().hit(x, y, true))
            event.getTarget().act(1);
        else
            event.getTarget().act(2);
    }
}
