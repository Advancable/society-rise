package com.advancable.socrise.Controller.GraphScreen;

import com.advancable.socrise.Model.Screens.Graph_Screen;
import com.badlogic.gdx.InputProcessor;


/**
 * @author Somkin Ivan
 *         Listener that processes the mouse wheel scrolling.
 */

public class Desktop_Graph_Listener implements InputProcessor {
    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return true;

    }

    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    public boolean touchDragged(int screenX, int screenY, int pointer) {

        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if (amount > 0) {
            Graph_Screen.zoomOut();
        } else {
            Graph_Screen.zoomIn();
        }
        return true;
    }


}
