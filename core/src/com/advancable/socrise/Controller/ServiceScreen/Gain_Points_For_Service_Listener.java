package com.advancable.socrise.Controller.ServiceScreen;

import com.advancable.socrise.Model.Screens.Service_Screen;
import com.badlogic.gdx.InputProcessor;

/**
 * @author Somkin Ivan
 * Listener that gives you points for swiping across an Actor.
 */
public class Gain_Points_For_Service_Listener implements InputProcessor {
    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(pointer==0)
            Service_Screen.getPoints(screenX,screenY);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(pointer==0)
            Service_Screen.movePointer(-100,-100);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(pointer==0)
            Service_Screen.getPoints(screenX,screenY);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
