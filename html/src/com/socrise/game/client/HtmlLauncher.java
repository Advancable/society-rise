package com.socrise.game.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.advancable.socrise.TheGame;

/**
 * HTML browser backend.
 */
public class HtmlLauncher extends GwtApplication {
        @Override
        public GwtApplicationConfiguration getConfig () {
                TheGame.setAssetsPath("android\\assets\\");
                TheGame.setLanguage("eng");
                return new GwtApplicationConfiguration(480, 320);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return TheGame.getInstance();

        }

        @Override
        public ApplicationListener createApplicationListener() {
                return null;
        }
}