package com.advancable.socrise.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.advancable.socrise.TheGame;

/**
 * PC backend.
 */
public class DesktopLauncher {
	public static LwjglApplicationConfiguration config;
	public static void main (String[] arg) {
		config = new LwjglApplicationConfiguration();
		System.setProperty("user.name", "English");
		new LwjglApplication(TheGame.getInstance(), config);
		config.addIcon("android\\assets\\logos\\logo.png", Files.FileType.Internal);
		config.addIcon("android\\assets\\logos\\logo2.png", Files.FileType.Internal);
		config.addIcon("android\\assets\\logos\\logo3.png", Files.FileType.Internal);
		config.title="Society Rise";
		config.width=1366;
		config.height=768;
		TheGame.setAssetsPath("android\\assets\\");
		TheGame.setLanguage("eng");
	}
}
